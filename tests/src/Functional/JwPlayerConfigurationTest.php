<?php

namespace Drupal\Tests\jw_player\Functional;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;

/**
 * Tests configuration of a jw player preset and creation of jw player content.
 *
 * @group jw_player
 */
class JwPlayerConfigurationTest extends BrowserTestBase {

  use FieldUiTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = array(
    'node',
    'jw_player',
    'file',
    'field',
    'field_ui',
    'block',
  );

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create jw_player content type.
    $this->drupalCreateContentType(array('type' => 'jw_player', 'name' => 'JW content'));
    // Place the breadcrumb, tested in fieldUIAddNewField().
    $this->drupalPlaceBlock('system_breadcrumb_block');
  }

  /**
   * Tests the jw player creation.
   */
  public function testJwPlayerCreation() {

    $admin_user = $this->drupalCreateUser(array(
      'administer site configuration',
      'administer JW Player presets',
      'administer nodes',
      'create jw_player content',
      'administer content types',
      'administer node fields',
      'administer node display',
      'access administration pages',
    ));
    $this->drupalLogin($admin_user);

    // Add a random "Cloud-Hosted Account Token".
    $edit = [
      'jw_player_hosting' => 'cloud',
      'cloud_player_library_url' => $cloud_library = 'https://content.jwplatform.com/libraries/' . $this->randomMachineName(8) . '.js',
    ];
    $this->drupalGet('admin/config/media/jw_player/settings');
    $this->submitForm($edit, t('Save configuration'));
    // Create a jw player preset.
    $edit = array(
      'label' => 'Test preset',
      'id' => 'test_preset',
      'description' => 'Test preset description',
      'settings[skin]' => 'bekle',
      'settings[mode]' => 'html5',
      'settings[width]' => 100,
      'settings[height]' => 100,
      'settings[advertising][client]' => 'vast',
      'settings[advertising][tag]' => 'www.example.com/vast',
      'settings[controlbar]' => 'bottom',
      'settings[mute]' => TRUE,
      'settings[autostart]' => TRUE,
    );
    $this->drupalGet('admin/config/media/jw_player/add');
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->pageTextContains('Saved the Test preset Preset.');
    // Make sure preset has correct values.
    $this->drupalGet('admin/config/media/jw_player/test_preset');
    $this->assertSession()->fieldValueEquals('label', 'Test preset');
    $this->assertSession()->fieldValueEquals('description', 'Test preset description');
    $this->assertSession()->fieldValueEquals('settings[mode]', 'html5');
    $this->assertSession()->fieldValueEquals('settings[skin]', 'bekle');
    $this->assertSession()->fieldValueEquals('settings[advertising][client]', 'vast');
    $this->assertSession()->fieldValueEquals('settings[advertising][tag]', 'www.example.com/vast');
    $this->assertSession()->fieldValueEquals('settings[controlbar]', 'bottom');
    $this->assertSession()->fieldValueEquals('settings[mute]', TRUE);
    $this->assertSession()->fieldValueEquals('settings[autostart]', TRUE);
    $this->assertSession()->fieldNotExists('settings[sharing]');

    // Create a JW player format file field in JW content type.
    static::fieldUIAddNewField('admin/structure/types/manage/jw_player', 'video', 'Video', 'file', array(), array('settings[file_extensions]' => 'mp4'));
    $this->drupalGet('admin/structure/types/manage/jw_player/display');
    $this->submitForm(array('fields[field_video][type]' => 'jwplayer_formatter'), t('Save'));
    $this->submitForm([], 'field_video_settings_edit');
    $edit = [
      'fields[field_video][settings_edit_form][settings][jwplayer_preset]' => 'test_preset',
    ];
    $this->submitForm($edit, t('Update'));
    $this->submitForm([], t('Save'));
    // Make sure JW preset is correct.
    $this->assertSession()->pageTextContains('Preset: Test preset');
    $this->assertSession()->pageTextContains('Dimensions: 100x100, uniform');
    $this->assertSession()->pageTextContains('Skin: bekle');
    $this->assertSession()->pageTextContains('Enabled options: Autostart, Mute');
    // Make sure the formatter reports correct dependencies.
    /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $view_display */
    $view_display = EntityViewDisplay::load('node.jw_player.default');
    $this->assertTrue(in_array('jw_player.preset.test_preset', $view_display->getDependencies()['config']));

    // Create a 'video' file, which has .mp4 extension.
    $text = 'Trust me I\'m a video';
    file_put_contents('temporary://myVideo.mp4', $text);
    // Create video content from JW content type.
    $edit = array(
      'title[0][value]' => 'Test video',
      'files[field_video_0]' => \Drupal::service('file_system')->realpath('temporary://myVideo.mp4'),
    );
    $this->drupalGet('node/add/jw_player');
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->pageTextContains('JW content Test video has been created.');

    $element = $this->getSession()->getPage()->find('css', 'video');
    $id =  $element->getAttribute('id');
    // Check the jw_player js, since the cloud player library url is set the
    // preset config is not applied.
    $this->assertSession()->responseContains('jw_player":{"players":{"' . $id . '":{"file":"' . str_replace('/', '\/', (\Drupal::service('file_url_generator')->generateAbsoluteString(\Drupal::token()->replace('public://[date:custom:Y]-[date:custom:m]/myVideo.mp4')))) . '"');
    // Make sure the hash is there.
    $this->assertEquals(1, preg_match('/jwplayer-[a-zA-Z0-9]{1,}$/', $id));
    // Check the library created because of cloud hosting.
    $this->assertSession()->responseContains('<script src="' . $cloud_library . '"></script>');
    // @todo Add test for advertising.
  }

  /**
   * Tests the UI for deletion of a preset.
   */
  public function testDelete() {
    $admin_user = $this->drupalCreateUser(array(
      'administer site configuration',
    ));
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/config/media/jw_player/add');

    // Create a preset.
    $this->submitForm([
      'label' => 'Test preset',
      'id' => 'test_preset',
      'settings[width]' => 100,
      'settings[height]' => 100,
    ], t('Save'));

    $this->clickLink(t('Delete'));
    $this->submitForm([], t('Delete'));
    $this->assertSession()->responseContains(t('The @entity-type %label has been deleted.', ['@entity-type' => 'jw player preset', '%label' => 'Test preset']));
    $this->assertEquals([], $this->xpath('//td[text()=@label]', ['@label' => 'Test preset']));
  }

}
