<?php

namespace Drupal\Tests\jw_player\Functional;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;
use Drupal\Tests\TestFileCreationTrait;

/**
 * Tests configuration of a jw player 7 preset and creation of jw player content.
 *
 * @group jw_player
 */
class JwPlayer7ConfigurationTest extends BrowserTestBase {

  use FieldUiTestTrait;
  use TestFileCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = array(
    'node',
    'jw_player',
    'file',
    'field',
    'field_ui',
    'block',
    'image',
    'link',
  );

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create jw_player content type.
    $this->drupalCreateContentType(array('type' => 'jw_player', 'name' => 'JW content'));
    // Place the breadcrumb, tested in fieldUIAddNewField().
    $this->drupalPlaceBlock('system_breadcrumb_block');
  }

  /**
   * Tests the jw player creation.
   */
  public function testJwPlayerCreation() {

    $admin_user = $this->drupalCreateUser(array(
      'administer site configuration',
      'administer JW Player presets',
      'administer nodes',
      'create jw_player content',
      'administer content types',
      'administer node fields',
      'administer node display',
      'access administration pages',
    ));
    $this->drupalLogin($admin_user);

    // Add a random "Cloud-Hosted Account Token".
    $edit = [
      'cloud_player_library_url' => $cloud_library = 'https://content.jwplatform.com/libraries/' . $this->randomMachineName(8) . '.js',
      'jw_player_hosting' => 'cloud',
      'jw_player_version' => 7,
    ];
    $this->drupalGet('admin/config/media/jw_player/settings');
    $this->submitForm($edit, t('Save configuration'));
    // Create a jw player preset.
    $edit = array(
      'label' => 'Test preset',
      'id' => 'test_preset',
      'description' => 'Test preset description',
      'settings[width]' => 100,
      'settings[height]' => 100,
      'settings[advertising][client]' => 'vast',
      'settings[advertising][tag]' => 'www.example.com/vast',
      'settings[advertising][tag_post]' => 'www.example.com/vast',
      'settings[advertising][skipoffset]' => 5,
      'settings[advertising][skipmessage]' => 'Skip ad in xx',
      'settings[advertising][skiptext]' => 'Skip',
      'settings[controlbar]' => 'bottom',
      'settings[mute]' => TRUE,
      'settings[autostart]' => TRUE,
      'settings[sharing]' => TRUE,
      'settings[sharing_sites][sites][linkedin][enabled]' => TRUE,
      'settings[sharing_sites][sites][email][enabled]' => TRUE,
    );
    $this->drupalGet('admin/config/media/jw_player/add');
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->pageTextContains('Saved the Test preset Preset.');
    // Make sure preset has correct values.
    $this->drupalGet('admin/config/media/jw_player/test_preset');
    $this->assertSession()->fieldValueEquals('label', 'Test preset');
    $this->assertSession()->fieldValueEquals('description', 'Test preset description');
    $this->assertSession()->fieldNotExists('settings[mode]');
    $this->assertSession()->fieldValueEquals('settings[preset_source]', 'drupal');
    $this->assertSession()->fieldValueEquals('settings[mute]', '1');
    $this->assertSession()->fieldValueEquals('settings[sharing]', '1');
    $this->assertSession()->fieldExists('settings[skin]');
    $this->assertSession()->fieldValueEquals('settings[advertising][client]', 'vast');
    $this->assertSession()->fieldValueEquals('settings[advertising][tag]', 'www.example.com/vast');
    $this->assertSession()->fieldValueEquals('settings[advertising][tag_post]', 'www.example.com/vast');
    $this->assertSession()->fieldValueEquals('settings[advertising][skipoffset]', 5);
    $this->assertSession()->fieldValueEquals('settings[advertising][skipmessage]', 'Skip ad in xx');
    $this->assertSession()->fieldValueEquals('settings[advertising][skiptext]', 'Skip');
    $this->assertSession()->fieldValueEquals('settings[controlbar]', 'bottom');
    $this->assertSession()->fieldValueEquals('settings[mute]', TRUE);
    $this->assertSession()->fieldValueEquals('settings[autostart]', TRUE);
    $this->assertSession()->fieldValueEquals('settings[sharing]', TRUE);
    $this->assertSession()->fieldValueEquals('settings[sharing_sites][sites][linkedin][enabled]', TRUE);
    $this->assertSession()->fieldValueEquals('settings[sharing_sites][sites][email][enabled]', TRUE);

    // Create a JW player format file field in JW content type.
    static::fieldUIAddNewField('admin/structure/types/manage/jw_player', 'video', 'Video', 'file', array(), array('settings[file_extensions]' => 'mp4'));
    // Create a Image field in JW content type.
    static::fieldUIAddNewField('admin/structure/types/manage/jw_player', 'image_preview', 'image_preview', 'image', [], []);
    $this->drupalGet('admin/structure/types/manage/jw_player/display');
    $this->submitForm(array('fields[field_video][type]' => 'jwplayer_formatter'), t('Save'));
    $this->submitForm([], 'field_video_settings_edit');
    // Set the image field as preview of the jw player video.
    $edit = [
      'fields[field_video][settings_edit_form][settings][jwplayer_preset]' => 'test_preset',
      'fields[field_video][settings_edit_form][settings][preview_image_field]' => 'node:jw_player|field_image_preview',
      'fields[field_video][settings_edit_form][settings][preview_image_style]' => 'medium',
    ];
    $this->submitForm($edit, t('Update'));
    $this->submitForm([], t('Save'));
    // Make sure JW preset is correct.
    $this->assertSession()->pageTextContains('Preset: Test preset');
    $this->assertSession()->pageTextContains('Dimensions: 100x100, uniform');
    $this->assertSession()->pageTextContains('Preview: image_preview (Medium');
    $this->assertSession()->pageTextContains('Enabled options: Autostart, Mute, Sharing');
    $this->assertSession()->pageTextContains('Sharing sites: Email, LinkedIn');
    // Make sure the formatter reports correct dependencies.
    /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $view_display */
    $view_display = EntityViewDisplay::load('node.jw_player.default');
    $this->assertTrue(in_array('jw_player.preset.test_preset', $view_display->getDependencies()['config']));

    // Create a 'video' file, which has .mp4 extension.
    $text = 'Trust me I\'m a video';
    file_put_contents('temporary://myVideo.mp4', $text);

    // Upload an image in the node.
    $images = $this->getTestFiles('image')[1];
    $this->drupalGet('node/add/jw_player');
    $this->submitForm([
      'files[field_image_preview_0]' => $images->uri,
    ], t('Upload'));

    // Create video content from JW content type.
    $edit = array(
      'title[0][value]' => 'Test video',
      'files[field_video_0]' => \Drupal::service('file_system')->realpath('temporary://myVideo.mp4'),
      'field_image_preview[0][alt]' => 'preview_image',
    );
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->pageTextContains('JW content Test video has been created.');

    $element = $this->getSession()->getPage()->find('css', 'video');
    $id =  $element->getAttribute('id');

    // Get the player js.
    $player_info = $this->xpath('//script[@data-drupal-selector="drupal-settings-json"]')[0]->getHtml();
    $decoded_info = json_decode($player_info, TRUE);

    // Assert the image and file.
    $image = \Drupal::service('file_url_generator')->generateAbsoluteString(\Drupal::token()->replace('public://styles/medium/public/[date:custom:Y]-[date:custom:m]/' . $images->filename));
    $this->assertTrue(strpos($decoded_info['jw_player']['players'][$id]['image'], $image) !== FALSE);
    $file = \Drupal::service('file_url_generator')->generateAbsoluteString(\Drupal::token()->replace('public://[date:custom:Y]-[date:custom:m]/myVideo.mp4'));
    $this->assertEquals($file, $decoded_info['jw_player']['players'][$id]['file']);

    // Make sure the hash is there.
    $this->assertEquals(1, preg_match('/jwplayer-[a-zA-Z0-9]{1,}$/', $id));
    // Check the library created because of cloud hosting.
    $this->assertSession()->responseContains('<script src="' . $cloud_library . '"></script>');

    // Change player hosting.
    $edit = [
      'jw_player_hosting' => 'self',
      'jw_player_key' => 'this_is_my_fancy_license_key',
    ];
    $this->drupalGet('admin/config/media/jw_player/settings');
    $this->submitForm($edit, t('Save configuration'));
    $this->drupalGet('node/1');
    $element = $this->getSession()->getPage()->find('css', 'video');
    $id =  $element->getAttribute('id');

    // Get the player js.
    $player_info = $this->xpath('//script[@data-drupal-selector="drupal-settings-json"]')[0]->getHtml();
    $decoded_info = json_decode($player_info, TRUE);

    // Assert the json has been updated.
    $this->assertEquals('100', $decoded_info['jw_player']['players'][$id]['width']);
    $this->assertEquals('100', $decoded_info['jw_player']['players'][$id]['height']);
    $this->assertTrue($decoded_info['jw_player']['players'][$id]['autostart']);
    $this->assertTrue($decoded_info['jw_player']['players'][$id]['mute']);
    $this->assertEquals([0 => 'email', 1 => 'linkedin'], $decoded_info['jw_player']['players'][$id]['sharing']['sites']);
    $this->assertEquals('5', $decoded_info['jw_player']['players'][$id]['advertising']['skipoffset']);
    $this->assertEquals('Skip ad in xx', $decoded_info['jw_player']['players'][$id]['advertising']['skipmessage']);
    $this->assertEquals('Skip', $decoded_info['jw_player']['players'][$id]['advertising']['skiptext']);

    // Check the library created because of cloud hosting.
    $this->assertSession()->responseNotContains('<script src="' . $cloud_library . '"></script>');

    // Test the formatter for a link field.
    static::fieldUIAddNewField('admin/structure/types/manage/jw_player', 'jw_link', 'JW link', 'link', [], []);
    $this->drupalGet('admin/structure/types/manage/jw_player/display');
    $this->submitForm(array('fields[field_jw_link][type]' => 'jwplayer_formatter'), t('Save'));
    // Add a new node.
    $this->drupalGet('node/add/jw_player');
    $edit = [
      'title[0][value]' => 'jw_link',
      'field_jw_link[0][uri]' => 'https://www.youtube.com/watch?v=mAAIfi0pYHw',
      'field_jw_link[0][title]' => 'Jw Player Drupal 7',
    ];
    $this->submitForm($edit, t('Save'));

    $node = $this->drupalGetNodeByTitle('jw_link');
    $this->drupalGet('node/' . $node->id());
    $element = $this->getSession()->getPage()->find('css', 'video');
    $id =  $element->getAttribute('id');
    // Get the player js.
    $player_info = (string) $this->xpath('//script[@data-drupal-selector="drupal-settings-json"]')[0]->getHtml();
    $decoded_info = json_decode($player_info, TRUE);
    // Check the link info is in the player js.
    $this->assertEquals('https://www.youtube.com/watch?v=mAAIfi0pYHw', $decoded_info['jw_player']['players'][$id]['file']);
  }

  /**
   * Tests the jw player license configuration.
   */
  public function testLicenseConfig() {
    $admin_user = $this->drupalCreateUser(array(
      'administer site configuration',
      'administer JW Player presets',
    ));
    $this->drupalLogin($admin_user);

    $edit = [
      'jw_player_hosting' => 'self',
      'jw_player_key' => $license_key = 'this_is_my_fancy_license_key',
    ];
    $this->drupalGet('admin/config/media/jw_player/settings');
    $this->submitForm($edit, t('Save configuration'));

    // Assert the key is saved.
    $this->drupalGet('admin/config/media/jw_player/settings');
    $this->assertSession()->fieldValueEquals('jw_player_key', $license_key);

    $edit = [
      'jw_player_hosting' => 'cloud',
      'cloud_player_library_url' => $cloud_url = 'this_is_my_fancy_cloud_url',
    ];
    $this->drupalGet('admin/config/media/jw_player/settings');
    $this->submitForm($edit, t('Save configuration'));

    // Assert the cloud url is saved and the license key is cleared.
    $this->drupalGet('admin/config/media/jw_player/settings');
    $this->assertSession()->fieldValueNotEquals('jw_player_key', $license_key);
    $this->assertSession()->fieldValueEquals('cloud_player_library_url', $cloud_url);
  }
}
